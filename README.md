## 引入
```go
import "gitee.com/golangx/validator"
```
## 校验函数:
```go
func Abs(value float64) float64
func BlackList(str, chars string) string
func ByteLength(str string, params ...string) bool
func CamelCaseToUnderscore(str string) string
func Contains(str, substring string) bool
func Count(array []interface{}, iterator ConditionIterator) int
func Each(array []interface{}, iterator Iterator)
func ErrorByField(e error, field string) string
func ErrorsByField(e error) map[string]string
func Filter(array []interface{}, iterator ConditionIterator) []interface{}
func Find(array []interface{}, iterator ConditionIterator) interface{}
func GetLine(s string, index int) (string, error)
func GetLines(s string) []string
func HasLowerCase(str string) bool
func HasUpperCase(str string) bool
func HasWhitespace(str string) bool
func HasWhitespaceOnly(str string) bool
func InRange(value interface{}, left interface{}, right interface{}) bool
func InRangeFloat32(value, left, right float32) bool
func InRangeFloat64(value, left, right float64) bool
func InRangeInt(value, left, right interface{}) bool
func IsASCII(str string) bool
func IsAlpha(str string) bool
func IsAlphanumeric(str string) bool
func IsBase64(str string) bool
func IsByteLength(str string, min, max int) bool
func IsCIDR(str string) bool
func IsCRC32(str string) bool
func IsCRC32b(str string) bool
func IsDNSName(str string) bool
func IsDataURI(str string) bool
func IsDialString(str string) bool
func IsDivisibleBy(str, num string) bool
func IsEmail(str string) bool
func IsExistingEmail(email string) bool
func IsFilePath(str string) (bool, int)
func IsFloat(str string) bool
func IsFullWidth(str string) bool
func IsHalfWidth(str string) bool
func IsHash(str string, algorithm string) bool
func IsHexadecimal(str string) bool
func IsHexcolor(str string) bool
func IsHost(str string) bool
func IsIP(str string) bool
func IsIPv4(str string) bool
func IsIPv6(str string) bool
func IsISBN(str string, version int) bool
func IsISBN10(str string) bool
func IsISBN13(str string) bool
func IsISO3166Alpha2(str string) bool
func IsISO3166Alpha3(str string) bool
func IsISO4217(str string) bool
func IsISO693Alpha2(str string) bool
func IsISO693Alpha3b(str string) bool
func IsIn(str string, params ...string) bool
func IsInRaw(str string, params ...string) bool
func IsInt(str string) bool
func IsJSON(str string) bool
func IsLatitude(str string) bool
func IsLongitude(str string) bool
func IsLowerCase(str string) bool
func IsMAC(str string) bool
func IsMD4(str string) bool
func IsMD5(str string) bool
func IsMagnetURI(str string) bool
func IsMongoID(str string) bool
func IsMultibyte(str string) bool
func IsNatural(value float64) bool
func IsNegative(value float64) bool
func IsNonNegative(value float64) bool
func IsNonPositive(value float64) bool
func IsNotNull(str string) bool
func IsNull(str string) bool
func IsNumeric(str string) bool
func IsPort(str string) bool
func IsPositive(value float64) bool
func IsPrintableASCII(str string) bool
func IsRFC3339(str string) bool
func IsRFC3339WithoutZone(str string) bool
func IsRGBcolor(str string) bool
func IsRequestURI(rawurl string) bool
func IsRequestURL(rawurl string) bool
func IsRipeMD128(str string) bool
func IsRipeMD160(str string) bool
func IsRsaPub(str string, params ...string) bool
func IsRsaPublicKey(str string, keylen int) bool
func IsSHA1(str string) bool
func IsSHA256(str string) bool
func IsSHA384(str string) bool
func IsSHA512(str string) bool
func IsSemver(str string) bool
func IsTiger128(str string) bool
func IsTiger160(str string) bool
func IsTiger192(str string) bool
func IsTime(str string, format string) bool
func IsType(v interface{}, params ...string) bool
func IsURL(str string) bool
func IsUTFDigit(str string) bool
func IsUTFLetter(str string) bool
func IsUTFLetterNumeric(str string) bool
func IsUTFNumeric(str string) bool
func IsUUID(str string) bool
func IsUUIDv3(str string) bool
func IsUUIDv4(str string) bool
func IsUUIDv5(str string) bool
func IsUnixTime(str string) bool
func IsUpperCase(str string) bool
func IsVariableWidth(str string) bool
func IsWhole(value float64) bool
func LeftTrim(str, chars string) string
func Map(array []interface{}, iterator ResultIterator) []interface{}
func Matches(str, pattern string) bool
func MaxStringLength(str string, params ...string) bool
func MinStringLength(str string, params ...string) bool
func NormalizeEmail(str string) (string, error)
func PadBoth(str string, padStr string, padLen int) string
func PadLeft(str string, padStr string, padLen int) string
func PadRight(str string, padStr string, padLen int) string
func PrependPathToErrors(err error, path string) error
func Range(str string, params ...string) bool
func RemoveTags(s string) string
func ReplacePattern(str, pattern, replace string) string
func Reverse(s string) string
func RightTrim(str, chars string) string
func RuneLength(str string, params ...string) bool
func SafeFileName(str string) string
func SetFieldsRequiredByDefault(value bool)
func SetNilPtrAllowedByRequired(value bool)
func Sign(value float64) float64
func StringLength(str string, params ...string) bool
func StringMatches(s string, params ...string) bool
func StripLow(str string, keepNewLines bool) string
func ToBoolean(str string) (bool, error)
func ToFloat(str string) (float64, error)
func ToInt(value interface{}) (res int64, err error)
func ToJSON(obj interface{}) (string, error)
func ToString(obj interface{}) string
func Trim(str, chars string) string
func Truncate(str string, length int, ending string) string
func TruncatingErrorf(str string, args ...interface{}) error
func UnderscoreToCamelCase(s string) string
func ValidateMap(inputMap map[string]interface{}, validationMap map[string]interface{}) (bool, error)
func ValidateStruct(s interface{}) (bool, error)
func WhiteList(str, chars string) string
type ConditionIterator
type CustomTypeValidator
type Error
func (e Error) Error() string
type Errors
func (es Errors) Error() string
func (es Errors) Errors() []error
type ISO3166Entry
type ISO693Entry
type InterfaceParamValidator
type Iterator
type ParamValidator
type ResultIterator
type UnsupportedTypeError
func (e *UnsupportedTypeError) Error() string
type Validator
```

## 例子
### IsURL
```go
println(validator.IsURL(`http://user@pass:domain.com/path/page`))
```
### IsType
```go
println(validator.IsType("Bob", "string"))
println(validator.IsType(1, "int"))
i := 1
println(validator.IsType(&i, "*int"))
```

IsType can be used through the tag `type` which is essential for map validation:
```go
type User	struct {
  Name string      `valid:"type(string)"`
  Age  int         `valid:"type(int)"`
  Meta interface{} `valid:"type(string)"`
}
result, err := validator.ValidateStruct(User{"Bob", 20, "meta"})
if err != nil {
	println("error: " + err.Error())
}
println(result)
```
### ToString
```go
type User struct {
	FirstName string
	LastName string
}

str := validator.ToString(&User{"John", "Juan"})
println(str)
```
### Each, Map, Filter, slices计数
Each iterates over the slice/array and calls Iterator for every item
```go
data := []interface{}{1, 2, 3, 4, 5}
var fn validator.Iterator = func(value interface{}, index int) {
	println(value.(int))
}
validator.Each(data, fn)
```
```go
data := []interface{}{1, 2, 3, 4, 5}
var fn validator.ResultIterator = func(value interface{}, index int) interface{} {
	return value.(int) * 3
}
_ = validator.Map(data, fn) // result = []interface{}{1, 6, 9, 12, 15}
```
```go
data := []interface{}{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
var fn validator.ConditionIterator = func(value interface{}, index int) bool {
	return value.(int)%2 == 0
}
_ = validator.Filter(data, fn) // result = []interface{}{2, 4, 6, 8, 10}
_ = validator.Count(data, fn) // result = 5
```
### Struct校验
如果要验证结构，可以对结构中的任何字段使用“valid”tag。在一个tag中与此字段一起使用的所有验证器都用逗号分隔。如果要跳过验证，请将“-”放在tag中。如果您需要自定义一个校验的tag，可以这样添加：
```go
validator.TagMap["duck"] = validator.Validator(func(str string) bool {
	return str == "duck"
})
```
下面是结构字段的常用验证列表 :
| tag | 函数 | 说明 |
|:--|:--|:--|
|email          |           IsEmail       | 是否邮箱地址 |
|url            |         IsURL            | 是否url地址    |
|alpha          |        IsAlpha           | 是否英文字母   |
|hexcolor       |     IsHexcolor           | 是否十六进制颜色,例如#ffffff |
|rgbcolor       |     IsRGBcolo            | 是否rgb颜色，例如rgb(255,255,255) |
|lowercase      |      IsLowerCase         | 是否小写字母                      |
|uppercase      |     IsUpperCase          | 是否大写字母                      |
|int            |     IsInt                | 是否int                          |
|float           |     IsFloat              | 是否float                         |
|null           |      IsNull              | 是否null                         |
|json           |      IsJSON              | 是否json字符串                    |
| ascii         |      IsASCII             | 是否ascii码                      |
|base64         |      IsBase64| 是否有效base64|
|ip             |                 IsIP  | 是否ip地址
|port           |            IsPort   | 是否端口号
|ipv4           |              IsIPv4 | 是否ipv4
|ipv6           |            IsIPv6   | 是否ipv6
|dns            |               IsDNSName | 是否DNS域名 |     
|host           |            IsHost |是否有效的host地址，(host文件地址)|
|mac            |          IsMAC |是否mac地址|
|latitude       |         IsLatitude |是否纬度 |
|longitude      |        IsLongitude | 是否经度 |
|semver         |      IsSemver| 是否规范的版本号格式, 例如： v1.0.0。主版本号.次版本号.修订号|
```

带参数的校验
```go
"range(min|max)": Range,
"length(min|max)": ByteLength,
"runelength(min|max)": RuneLength,
"stringlength(min|max)": StringLength,
"matches(pattern)": StringMatches,
"in(string1|string2|...|stringN)": IsIn,
"rsapub(keylength)" : IsRsaPub,
"minstringlength(int): MinStringLength,
"maxstringlength(int): MaxStringLength,
```

带参数校验类型
```go
"type(type)": IsType,
```

例子:
```go
type Post struct {
	Title    string `valid:"alphanum,required"`
	Message  string `valid:"duck,ascii"`
	Message2 string `valid:"animal(dog)"`
	AuthorIP string `valid:"ipv4"`
	Date     string `valid:"-"`
}
post := &Post{
	Title:   "My Example Post",
	Message: "duck",
	Message2: "dog",
	AuthorIP: "123.234.54.3",
}

// 添加一个struct的校验tag   `valid:"duck"`
validator.TagMap["duck"] = validator.Validator(func(str string) bool {
	return str == "duck"
})

// 添加一个带参数的struct的校验tag   `valid:"animal(string)"`
validator.ParamTagMap["animal"] = validator.ParamValidator(func(str string, params ...string) bool {
    species := params[0]
    return str == species
})
validator.ParamTagRegexMap["animal"] = regexp.MustCompile("^animal\\((\\w+)\\)$")

result, err := validator.ValidateStruct(post)
if err != nil {
	println("error: " + err.Error())
}
println(result)
```
### Map 校验

如果要验证map，可以使用要验证的映射和包含ValidateStruct中使用的相同标记的验证映射，这两个map必须为`map[string]interface{}`类型

So here is small example of usage:
```go
var mapTemplate = map[string]interface{}{
	"name":"required,alpha",
	"family":"required,alpha",
	"email":"required,email",
	"cell-phone":"numeric",
	"address":map[string]interface{}{
		"line1":"required,alphanum",
		"line2":"alphanum",
		"postal-code":"numeric",
	},
}

var inputMap = map[string]interface{}{
	"name":"Bob",
	"family":"Smith",
	"email":"foo@bar.baz",
	"address":map[string]interface{}{
		"line1":"",
		"line2":"",
		"postal-code":"",
	},
}

result, err := validator.ValidateMap(inputMap, mapTemplate)
if err != nil {
	println("error: " + err.Error())
}
println(result)
```

### 白名单
```go
// Remove all characters from string ignoring characters between "a" and "z"
println(validator.WhiteList("a3a43a5a4a3a2a23a4a5a4a3a4", "a-z") == "aaaaaaaaaaaa")
```

### 自定义校验函数
Custom validation using your own domain specific validators is also available - here's an example of how to use it:
```go
import "gitee.com/golangx/validator"

type CustomByteArray [6]byte // custom types are supported and can be validated

type StructWithCustomByteArray struct {
  ID              CustomByteArray `valid:"customByteArrayValidator,customMinLengthValidator"` // multiple custom validators are possible as well and will be evaluated in sequence
  Email           string          `valid:"email"`
  CustomMinLength int             `valid:"-"`
}

validator.CustomTypeTagMap.Set("customByteArrayValidator", func(i interface{}, context interface{}) bool {
  switch v := context.(type) { // you can type switch on the context interface being validated
  case StructWithCustomByteArray:
    // you can check and validate against some other field in the context,
    // return early or not validate against the context at all – your choice
  case SomeOtherType:
    // ...
  default:
    // expecting some other type? Throw/panic here or continue
  }

  switch v := i.(type) { // type switch on the struct field being validated
  case CustomByteArray:
    for _, e := range v { // 检查字节数组是否不为空，即不是全部为零
      if e != 0 {
        return true
      }
    }
  }
  return false
})
validator.CustomTypeTagMap.Set("customMinLengthValidator", func(i interface{}, context interface{}) bool {
  switch v := context.(type) { // 根据一个字段校验另外一个字段
  case StructWithCustomByteArray:
    return len(v.ID) >= v.CustomMinLength
  }
  return false
})
```

### 遍历访问 Error()
默认情况下，Error（）返回单个字符串中的所有错误。要访问每个错误，可以执行以下操作：
```go
  if err != nil {
    errs := err.(validator.Errors).Errors()
    for _, e := range errs {
      fmt.Println(e.Error())
    }
  }
```

### 自定义错误信息
通过添加`~`分隔符，可以通过注释支持自定义错误消息:
```go
type Ticket struct {
  Id        int64     `json:"id"`
  FirstName string    `json:"firstname" valid:"required~First name is blank"`
}
```